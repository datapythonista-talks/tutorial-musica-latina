# Introducción al análisis de datos en Python

El video del tutorial está disponible en [Peertube](https://p.eertu.be/videos/watch/191a5541-a075-4c73-a204-61c3b8d9055f).

El notebook se puede usar en remote utilizando Binder:

[![Binder](https://mybinder.org/badge.svg)](https://mybinder.org/v2/gh/datapythonista/tutorial-musica-latina/master)

Autor: [Marc Garcia](https://datapythonista.me/)
